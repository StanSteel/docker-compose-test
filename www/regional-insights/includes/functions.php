<?php
function isValidImage($filename) {
	// strrchr finds the last occurence of a character in a string
	$extension = substr(strrchr($filename, '.'), 1);
	
	if ($extension == 'png' || $extension == 'PNG') {
		return TRUE;
	}
	return FALSE;
}

function isValidDirectory($directory) {
	if (substr($directory, 0, 1) == '.') {
		return FALSE;
	}
	
	return TRUE;
}