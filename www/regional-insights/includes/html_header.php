<!DOCTYPE html>
<html lang="en">
	<head>
    	<meta charset="utf-8">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
    	<title><?php echo (isset($page_title))?$page_title:'aWhere Insights Demo';?></title>
		<link rel="shortcut icon" href="/favicon.ico" type="image/png" />
		<link href="css/normalize.css" rel="stylesheet">
		<!--<link href="//cloud.webtype.com/css/d55d7b10-d805-4ddd-ac09-3b9f2e4dbe14.css" rel="stylesheet" type="text/css" />-->
		<link href="//fast.fonts.net/cssapi/51f90e29-7f8c-4723-b10e-d974a3f29a6a.css" rel="stylesheet" type="text/css" />
		<link href="css/font-awesome.min.css" rel="stylesheet">
		<link href="css/main.css" rel="stylesheet">
  	</head>
  	<body>
	<header>
		<a href="./"><img src="images/aWhereLogo.png"></a>
	</header>
