<?php
include_once('functions.php');

$countries = scandir('../charts-maps');

$chartsMapsDataByCountry = array();
$chartsMapsDataByCrop    = array();
$missingFileInfo         = array();

foreach ($countries as $countryDirectory) {
	// Skip the directory if it starts with '.'
	if (!isValidDirectory($countryDirectory)) {
		continue;
	}
	
	/* Split up the Directory name to extract the Country
	 * For example: "Argentina20160721to0819" is put into an array called $dirmatches
	 * [0] => Argentina20160721to0819
	 * [1] => Argentina
	 * [2] => 20160721
	 * [3] => 0819
	 */
	preg_match('/([A-Za-z]*)([0-9]*)to([0-9]{4})/', $countryDirectory, $countryDirectoryPieces);
	$country = $countryDirectoryPieces[1];
	
	// If the Country is not already in the chartsMapDataByCountry array, add it
	if (!array_key_exists($country, $chartsMapsDataByCountry)) {
		$chartsMapsDataByCountry[$country] = array();
	}
	
	
	$countryImages = scandir('../charts-maps/' . $countryDirectory);
	
	foreach ($countryImages as $filename) {
		if (!isValidImage($filename)) {
			continue;
		}
		
		preg_match('/([A-Za-z]*)\-*(' . $country . ')([0-9]{4})([A-Za-z_5095]*)([0-9]{4})to([0-9]{4})_([A-Za-z]*).png/', $filename, $filenamePieces);
		// TODO: Rename to $endingDate, move into a function
		$endingDate = $filenamePieces[3] . '-' . substr($filenamePieces[6], 0, 2) . '-' . substr($filenamePieces[6], 2);
		
		if (!array_key_exists($filenamePieces[4], $chartsMapsDataByCountry[$country])) {
			$chartsMapsDataByCountry[$country][$filenamePieces[4]] = array();
		}
		
		if (!array_key_exists($endingDate, $chartsMapsDataByCountry[$country][$filenamePieces[4]])) {
			$chartsMapsDataByCountry[$country][$filenamePieces[4]][$endingDate] = array(
				'bycountry' => array(),
				'bycrop' => array()
			);
		}
		
		if (empty($filenamePieces[4])) {
			array_push($missingFileInfo, $filenamePieces);
		}
		
		if (empty($filenamePieces[1])) {
			if ($filenamePieces[7] == 'Chart') {
				$chartsMapsDataByCountry[$country][$filenamePieces[4]][$endingDate]['bycountry']['chart'] = $countryDirectory . '/' . $filenamePieces[0];
			} elseif ($filenamePieces[7] == 'Map') {
				$chartsMapsDataByCountry[$country][$filenamePieces[4]][$endingDate]['bycountry']['map'] = $countryDirectory . '/' . $filenamePieces[0];
			}
		} else {
			if ($filenamePieces[7] == 'Chart') {
				$chartsMapsDataByCountry[$country][$filenamePieces[4]][$endingDate]['bycrop'][$filenamePieces[1]]['chart'] = $countryDirectory . '/' . $filenamePieces[0];
			} elseif ($filenamePieces[7] == 'Map') {
				$chartsMapsDataByCountry[$country][$filenamePieces[4]][$endingDate]['bycrop'][$filenamePieces[1]]['map'] = $countryDirectory . '/' . $filenamePieces[0];
			}
		}
		// Sort the array by key in reverse order
		krsort($chartsMapsDataByCountry[$country][$filenamePieces[4]]);
	}
}

foreach ($chartsMapsDataByCountry as $country => $dataByChart) {
	foreach ($dataByChart as $chart => $dataByDate) {
		foreach ($dataByDate as $date => $by) {
			foreach ($by['bycrop'] as $crop => $data) {
				
				if (!array_key_exists($crop, $chartsMapsDataByCrop))
					$chartsMapsDataByCrop[$crop] = array();
				if (!array_key_exists($chart, $chartsMapsDataByCrop[$crop]))
					$chartsMapsDataByCrop[$crop][$chart] = array();
				if (!array_key_exists($date, $chartsMapsDataByCrop[$crop][$chart]))
					$chartsMapsDataByCrop[$crop][$chart][$date] = array();
				$chartsMapsDataByCrop[$crop][$chart][$date][$country] = $data;
				
			}
		}
	}
}

header("Content-Type: text/javascript");
echo 'var chartsMapsByCountry = ' . json_encode($chartsMapsDataByCountry) . ';';
echo 'var chartsMapsByCrop = ' . json_encode($chartsMapsDataByCrop) . ';';