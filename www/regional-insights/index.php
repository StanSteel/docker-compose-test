<?php

// This app is a collection of JavaScript widgets each of which encapsulates the required HTML for that widget.
// All the action is in JS and CSS

$page_title = 'aWhere Insights Demo';
$javascripts = array('includes/chartsMapsProcessor.php', //scans the /charts-maps/ directory for all the available imagery, renders it as a JS Array
					 'js/CountryPicker.js',      // Country Picker Control
					 'js/CountryInterface.js',   // Manages the main interface for viewing by-country
					 'js/TimelineControl.js',    // Timeline Control
					 'js/ImageFilterControl.js', // Chooser Control
					 'js/init.js');  			 // go go gadget insights tool

include_once('./includes/html_header.php');
?>

<a href="./" id="StartOver">&laquo; Start Over</a>
<div id="Loading">
	<img src="images/loader.gif">
</div>

<?php
	include_once("./includes/html_footer.php");
?>
