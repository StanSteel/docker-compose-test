var aWhereCountryPicker = function(){
	var pickerId;
	var siblingPickerIds;
	this.init = function(setId,siblingPickers){
		pickerId = setId;
		if(siblingPickers!=undefined) this.setSiblingPickers(siblingPickers);

		var preloadMap = new Image();
			preloadMap.src = 'images/basemap.png';
		var preloadMap2 = new Image();
			preloadMap2.src = 'images/basemap-micro.png'; 

		$('body').append('<div id="'+pickerId+'" class="asstartbutton">\
	<img id="WorldMapBase" src="images/basemap.png" width="100%">\
	<img id="MicroMap" src="images/basemap-micro.png" width="100%">\
	<img id="WorldMapLabels" src="images/labels.png" border="0" width="100%" usemap="#CountrySelectorImageMap"/>\
	<div id="MapRegionalHover" class=""></div>\
	<h2>Choose a region to explore</h2>\
	<a href="javascript:void(0);" class="startlink">Regional Insight</a>\
	<a href="javascript:void(0);" class="changelink">Change Region &raquo;</a>\
</div>');

		$('body').append('<map name="CountrySelectorImageMap" id="CountrySelectorImageMap">\
	<area id="MapSelect_Argentina" alt="" href="javascript:void(0)" shape="rect" coords="122,403,225,427" style="outline:none;" target="_self"     />\
	<area id="MapSelect_Brazil" alt="" href="javascript:void(0)" shape="rect" coords="123,282,196,306" style="outline:none;" target="_self"     />\
	<area id="MapSelect_BurkinaFaso" alt="" href="javascript:void(0)" shape="rect" coords="318,227,437,251" style="outline:none;" target="_self"     />\
	<area id="MapSelect_CentralEastAfrica" alt="" href="javascript:void(0)" shape="rect" coords="650,265,804,291" style="outline:none;" target="_self"     />\
	<area id="MapSelect_China" alt="" href="javascript:void(0)" shape="rect" coords="931,40,1015,66" style="outline:none;" target="_self"     />\
	<area id="MapSelect_Colombia" alt="" href="javascript:void(0)" shape="rect" coords="12,261,115,285" style="outline:none;" target="_self"     />\
	<area id="MapSelect_Ethiopia" alt="" href="javascript:void(0)" shape="rect" coords="672,193,751,217" style="outline:none;" target="_self"     />\
	<area id="MapSelect_Europe" alt="" href="javascript:void(0)" shape="rect" coords="393,30,477,59" style="outline:none;" target="_self"     />\
	<area id="MapSelect_Guatemala" alt="" href="javascript:void(0)" shape="rect" coords="0,178,103,202" style="outline:none;" target="_self"     />\
	<area id="MapSelect_Honduras" alt="" href="javascript:void(0)" shape="rect" coords="0,203,103,227" style="outline:none;" target="_self"     />\
	<area id="MapSelect_India" alt="" href="javascript:void(0)" shape="rect" coords="700,99,768,125" style="outline:none;" target="_self"     />\
	<area id="MapSelect_Indonesia" alt="" href="javascript:void(0)" shape="rect" coords="768,230,866,256" style="outline:none;" target="_self"     />\
	<area id="MapSelect_Mali" alt="" href="javascript:void(0)" shape="rect" coords="355,139,418,163" style="outline:none;" target="_self"     />\
	<area id="MapSelect_MexicotoPanama" alt="" href="javascript:void(0)" shape="rect" coords="172,164,323,188" style="outline:none;" target="_self"     />\
	<area id="MapSelect_Nicaragua" alt="" href="javascript:void(0)" shape="rect" coords="0,230,103,254" style="outline:none;" target="_self"     />\
	<area id="MapSelect_Niger" alt="" href="javascript:void(0)" shape="rect" coords="339,168,414,192" style="outline:none;" target="_self"     />\
	<area id="MapSelect_Nigeria" alt="" href="javascript:void(0)" shape="rect" coords="339,198,418,222" style="outline:none;" target="_self"     />\
	<area id="MapSelect_NorthernAfrica" alt="" href="javascript:void(0)" shape="rect" coords="291,96,447,120" style="outline:none;" target="_self"     />\
	<area id="MapSelect_SahelAfrica" alt="" href="javascript:void(0)" shape="rect" coords="655,162,723,190" style="outline:none;" target="_self"     />\
	<area id="MapSelect_SouthAfrica" alt="" href="javascript:void(0)" shape="rect" coords="583,385,705,413" style="outline:none;" target="_self"     />\
	<area id="MapSelect_Thailand" alt="" href="javascript:void(0)" shape="rect" coords="885,214,976,240" style="outline:none;" target="_self"     />\
	<area id="MapSelect_Uganda" alt="" href="javascript:void(0)" shape="rect" coords="676,223,755,247" style="outline:none;" target="_self"     />\
	<area id="MapSelect_Ukraine" alt="" href="javascript:void(0)" shape="rect" coords="632,19,716,48" style="outline:none;" target="_self"     />\
	<area id="MapSelect_USGrains" alt="" href="javascript:void(0)" shape="rect" coords="160,27,282,56" style="outline:none;" target="_self"     />\
	<area id="MapSelect_Vietnam" alt="" href="javascript:void(0)" shape="rect" coords="901,147,992,173" style="outline:none;" target="_self"     />\
	<area id="MapSelect_WesternAfrica" alt="" href="javascript:void(0)" shape="rect" coords="386,256,505,280" style="outline:none;" target="_self"     />\
</map>');
	//REMOVED: <area id="MapSelect_Mexico" alt="" href="javascript:void(0)" shape="rect" coords="113,118,191,142" style="outline:none;" target="_self"     />\

		$('#'+pickerId).on('click',this.go);

	}
	this.setSiblingPickers = function(pickers){
		siblingPickerIds = pickers;
	}
	this.go = function(){
		aWhere.CountryInterface.close();
		for(var i=0;i<siblingPickerIds.length;i++){
			$('#'+siblingPickerIds[i]).hide();
		}
		$('#'+pickerId).removeClass('asstartbutton').removeClass('activereset');
		$('#'+pickerId).css('left',(($(window).width()-1028)*.5)).css('top',150);
		$('#'+pickerId).off('click',aWhere.CountryPicker.go);
		$('#CountrySelectorImageMap area').mouseover(highlight);
		$('#CountrySelectorImageMap area').mouseout(clearHighlight);
		$('#CountrySelectorImageMap area').click(chooseCountry);
		$('#StartOver').html('&laquo; Start Over').removeClass('moveleft').addClass('on');
	}
	function standby(){
		$('#'+pickerId).addClass('activereset');
		$('#'+pickerId).css('left',($(window).width()-105)).css('top',3);
		$('#'+pickerId).on('click',aWhere.CountryPicker.go);
		$('#CountrySelectorImageMap area').off('mouseover',highlight);
		$('#CountrySelectorImageMap area').off('mouseout',clearHighlight);
		$('#CountrySelectorImageMap area').off('click',chooseCountry);
		$('#StartOver').removeClass('on').addClass('moveleft').html('&laquo; Start Over&nbsp;&nbsp;•');
		clearHighlight();
	}

	function chooseCountry(){
		var country = this.id.split('_')[1];
		standby();
		aWhere.CountryInterface.go(country);
	}

	function highlight(){
		var country = this.id.split('_')[1];
		clearHighlight();
		$('#MapRegionalHover').addClass(country);
	}
	function clearHighlight(){
		$('#MapRegionalHover').removeClass($('#MapRegionalHover').attr('class'))
	}


}
