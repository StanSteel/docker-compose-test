var aWhereCountryInterface = function(){ 
	
	var CurrentCountry = null; 
	var CurrentDate = null; 
	var CurrentCrop = null; 
	var CurrentChart = null; 
	var mapsAvail = []; 
	var datesAvail = []; 
	var cropsAvail = []; 
	
	var preloads = [];
	
	var TimelineControl = new aWhereTimelineControl(this,'#CountryExplore .timescale'); 
		TimelineControl.setListener('dateChange',changeDate); 

	var CropChooserControl; 
	var ChartChooserControl; 
	
	this.init = function(){ 
		$('body').append('<article id="CountryExplore">\
	<div class="content">\
		<h1></h1>\
		<div class="chart-chooser"></div>\
		<h2></h2>\
		<p></p>\
		<div class="timescale">\
		</div>\
		<div class="display">\
			<div class="map">\
				<div class="img"></div>\
				<span class="caption"></span>\
			</div>\
			<div class="chart">\
				<div class="img"></div>\
				<span class="caption"></span>\
			</div>\
		</div>\
	</div>\
</article>'); 
	} 
	
	this.go = function(country){	
	
		CurrentCountry = country; 
		$('#CountryExplore').addClass('on'); 
		
		for(var i in chartsMapsByCountry[CurrentCountry]){ 
			mapsAvail[mapsAvail.length] = i; 
			for(var d in chartsMapsByCountry[CurrentCountry][i]){ 
				if(datesAvail.indexOf(d)<0){ 
					datesAvail[datesAvail.length] = d; 
				} 
			} 
		} 	
		datesAvail.sort(); 
		for(var i in chartsMapsByCountry[CurrentCountry][mapsAvail[0]][datesAvail[0]].bycrop){ 
			cropsAvail[cropsAvail.length] = i; 
		} 
		
		$('#CountryExplore h1').html(aWhere.config.CountryNameDefinition[CurrentCountry]); 
		
		buildDropdownChoosers();
		TimelineControl.create(datesAvail);
		changeChart();
		
	} 
	
	this.close = function(){ 
		CurrentCountry = null; 
		CurrentDate = null; 
		CurrentCrop = null; 
		mapsAvail = []; 
 		datesAvail = []; 
		cropsAvail = []; 
	
		TimelineControl.destroy(); 
		if(CropChooserControl!==undefined) CropChooserControl.destroy();  
	 	if(ChartChooserControl!==undefined) ChartChooserControl.destroy(); 
	
		$('#CountryExplore').removeClass('on');
	} 
	
	this.getCurrentCountry = function(){ 
		return CurrentCountry; 
	} 
	
	this.getCurrentDate = function(){ 
		return CurrentDate; 
	}
	 
	this.getCurrentCrop = function(){ 
		return CurrentCrop; 
	} 
	

	function buildDropdownChoosers(){ 
		
		CropChooserControl = new aWhereImageFilterControl(this,'#CountryExplore .chart-chooser'); 
	 	ChartChooserControl = new aWhereImageFilterControl(this,'#CountryExplore .chart-chooser');  
		
		var chartConfig = { 
			'chooserType':'Chart',
			'chooserLabel':'Metric',
			'items':[]
		}
		
		var cropConfig = { 
			'chooserType':'Crop',
			'chooserLabel':'Focus',
			'items':[]
		}
		
		for(var i in aWhere.config.ChartsMapsDefinition){ 
			if((mapsAvail.indexOf(i)>=0 || aWhere.config.ChartsMapsDefinition[i].isGroup==true) && aWhere.config.ChartsMapsDefinition[i].show==true){ 
				
				if(aWhere.config.ChartsMapsDefinition[i].isGroup==true){ 
					chartConfig.items.push({'type':'group','label':aWhere.config.ChartsMapsDefinition[i].name,'value':''}); 
					continue;
				}
				
				chartConfig.items.push({'type':'item','label':aWhere.config.ChartsMapsDefinition[i].name,'value':i}); 
				
			} 
		} 

		cropConfig.items.push({'type':'item','label':'Entire Country','value':'All'}); 
		for(var i=0,il=cropsAvail.length;i<il;i++){ 
			cropConfig.items.push({'type':'item','label':cropsAvail[i],'value':cropsAvail[i]}); 
		} 
		ChartChooserControl.create(chartConfig); 
		CropChooserControl.create(cropConfig); 
		
		ChartChooserControl.setListener('selectionChange',handleChooserChange); 
		CropChooserControl.setListener('selectionChange',handleChooserChange); 
	} 
	
	function preloadHistoricalImages(){ 
		var pindex; 
		var chart = ChartChooserControl.getValue(); 
	
		if(CurrentCrop===null){ 
			for(var date in chartsMapsByCountry[CurrentCountry][chart]){ 
				pindex = preloads.length; 
				preloads[pindex] = new Image(); 
				preloads[pindex].src = 'charts-maps/'+chartsMapsByCountry[CurrentCountry][chart][date].bycountry.map; 
				pindex = preloads.length; 
				preloads[pindex] = new Image(); 
				preloads[pindex].src = 'charts-maps/'+chartsMapsByCountry[CurrentCountry][chart][date].bycountry.chart;
			} 
		} else { 
			for(var date in chartsMapsByCountry[CurrentCountry][chart]){ 
				pindex = preloads.length; 
				preloads[pindex] = new Image(); 
				preloads[pindex].src = 'charts-maps/'+chartsMapsByCountry[CurrentCountry][chart][date].bycrop[CurrentCrop].map;
				pindex = preloads.length; 
				preloads[pindex] = new Image(); 
				preloads[pindex].src = 'charts-maps/'+chartsMapsByCountry[CurrentCountry][chart][date].bycrop[CurrentCrop].chart;
			}
		} 
	} 
	
	function changeChart(timeChangeOnly){
		var chart = ChartChooserControl.getValue(); 
		if(timeChangeOnly!==true) preloadHistoricalImages(); 
		var data = chartsMapsByCountry[CurrentCountry][chart]; 
		var dates = [],chartimg,mapimg; 
		for(var i in data){ 
			dates[dates.length] = i; 
		} 
		TimelineControl.setActiveDates(dates);
		if(CurrentDate===null) CurrentDate = dates[0]; 
		if(CurrentCrop===null){ 
			chartimg = data[CurrentDate].bycountry.chart; 
			mapimg = data[CurrentDate].bycountry.map; 
		} else { 
			chartimg = data[CurrentDate].bycrop[CurrentCrop].chart; 
			mapimg = data[CurrentDate].bycrop[CurrentCrop].map; 
		} 

		$('#CountryExplore .display .img').off('click',zoomImages); 
		if(timeChangeOnly==true){ 
			$('#CountryExplore .display .map .img').html('<img src="charts-maps/'+mapimg+'" class="map" width="100%">');
			$('#CountryExplore .display .chart .img').html('<img src="charts-maps/'+chartimg+'" class="chart" width="100%">'); 
		} else { 
			$('#CountryExplore > .content > h2').fadeOut(400,function(){ 
				$('#CountryExplore > .content > h2').html(aWhere.config.ChartsMapsDefinition[chart].name+((CurrentCrop===null)?'':' ('+CurrentCrop+')')).fadeIn(); 
			}); 
			$('#CountryExplore > .content > p').fadeOut(400,function(){ 
				$('#CountryExplore > .content > p').html(aWhere.config.ChartsMapsDefinition[chart].description).fadeIn(); 
			}); 
			$('#CountryExplore .display .map').fadeOut(400,function(){ 
				$('#CountryExplore .display .map .caption').html( ((CurrentCrop!==null)?'Key growing areas for '+CurrentCrop+' are outlined in black.':'') ); 
				$('#CountryExplore .display .map .img').html('<img src="charts-maps/'+mapimg+'" class="map" width="100%">');
				$('#CountryExplore .display .map').fadeIn();
			}); 
			$('#CountryExplore .display .chart').fadeOut(400,function(){ 
				$('#CountryExplore .display .chart .caption').html( ((CurrentCrop!==null)?'Histogram is specific to key '+CurrentCrop+' growing areas.':'') ); 
				$('#CountryExplore .display .chart .img').html('<img src="charts-maps/'+chartimg+'" class="chart" width="100%">'); 
				$('#CountryExplore .display .chart').fadeIn(); 
			}); 
		} 
		$('#CountryExplore .display .img').on('click',zoomImages); 
	}
	
	// LISTENERS 
	function changeDate(e){ 
		CurrentDate = e.newDate; 
		changeChart(true); 
	} 
	
	function handleChooserChange(e){ 
		if(e.chooserType=='Crop'){ 
			if(e.newValue=='All'){ 
				CurrentCrop = null; 
			} else { 
				CurrentCrop = e.newValue; 
			} 
		} 
		changeChart(); 
	} 
	
	function zoomImages(){ 
		if($('#CountryExplore .display').hasClass('zoomed')){ 
			$('#CountryExplore .display').removeClass('zoomed'); 
		} else { 
			$('#CountryExplore .display').addClass('zoomed'); 
			if($(this).parent().hasClass('chart')){ 
				setTimeout(function(){ $('html, body').animate({scrollTop: $("#CountryExplore .display .chart img").offset().top}, 1000)},1000);
			} else if($(this).parent().hasClass('map')){ 
				setTimeout(function(){ $('html, body').animate({scrollTop: $("#CountryExplore .display .map img").offset().top}, 1000)},1000);
			}
		} 
	} 
	
} 