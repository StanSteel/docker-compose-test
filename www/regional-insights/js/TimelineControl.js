var aWhereTimelineControl = function(owner,targetParent){ 

	var eventListeners = { 
							dateChange: function(){ },
							autoplayStart: function(){ },
							autoplayStop: function(){ },
							autoplayAdvance: function(){ },
	 					 }; 

	var autoPlayInterval; 
	if(!$('#TimescaleTooltip').length) $('body').append('<div id="TimescaleTooltip"><i class="fa fa-play fa-rotate-270"></i><span></span></div>'); 
	if(!$('#TimescaleInfotip').length) $('body').append('<div id="TimescaleInfotip"><i class="fa fa-play fa-rotate-270"></i><span>Each image aggregates over a rolling four-week period ending on this date.</span></div>'); 
	
	this.create = function(datesAvail){ 
		
		$(targetParent).html('<span class="label">Time &nbsp;&nbsp;<i class="fa fa-info-circle"></i></span><span class="buttons"></span><span class="player"><a href="javascript:void(0)" class="TimePlayer"><i class="fa fa-play"></i></a></span>'); 
		
		var anchor; 
		for(var i=0,il=datesAvail.length;i<il;i++){
			anchor = '<a href="javascript:void(0);" id="date_'+datesAvail[i]+'" ';
			if(owner.getCurrentDate()==datesAvail[i]) anchor+=' class="current" '; 
			anchor+='data-enddate="'+datesAvail[i].split("-")[1]+'/'+datesAvail[i].split("-")[2]+'/'+datesAvail[i].split("-")[0]+'">&nbsp;</a>'
		
			$(targetParent+' .buttons').append(anchor); 
		} 
		$(targetParent+' .buttons a').on('mouseover',showTimescaleTip); 
		$(targetParent+' .buttons a').on('mouseout',hideTimescaleTip); 
		$(targetParent+' .buttons a').on('click',changeDate); 
		$(targetParent+' .TimePlayer').on('click',autoPlayHistory); 
		if(owner.getCurrentDate()===null){ 
			$(targetParent+' .buttons a').last().addClass('current'); 
		} 
		$(targetParent+' .label i').on('mouseover',showTimeScaleInfo); 
		$(targetParent+' .label i').on('mouseout',hideTimeScaleInfo); 
	} 
	this.destroy = function(){ 
		$(targetParent+' .TimePlayer').off('click',autoPlayHistory); 
		$(targetParent+' .TimePlayer').off('click',stopAutoPlayHistory); 
		$(targetParent+' .TimePlayer i.fa').removeClass('fa-pause').removeClass('fa-play').addClass('fa-play'); 
		$(targetParent+' .buttons a').off('mouseover',showTimescaleTip); 
		$(targetParent+' .buttons a').off('mouseout',hideTimescaleTip); 
		$(targetParent+' .buttons a').off('click',changeDate); 
		$(targetParent+' .label i').off('mouseover',showTimeScaleInfo); 
		$(targetParent+' .label i').off('mouseout',hideTimeScaleInfo); 
		$('#TimescaleTooltip').fadeOut(); 
		$(targetParent).html(''); 		
	} 
	
	this.setListener = function(e,func){ 
		eventListeners[e] = func; 
	} 
	this.setActiveDates = function(dates){ 
		$.each($(targetParent+' .buttons a'),function(index,object){ 
			var thisdate = object.id.split('_')[1]; 
			if(dates.indexOf(thisdate)<0){ 
				$(object).addClass('unavailable'); 
			} else { 
				$(object).removeClass('unavailable'); 
			} 
		}); 
	} 

	function changeDate(){ 
		var date = this.id.split('_')[1]; 
		$('a.current').removeClass('current'); 
		$('#'+this.id).addClass('current'); 
		eventListeners.dateChange({'event':'dateChange','newDate':date}); 
	} 

	function autoPlayHistory(){ 
		autoPlayInterval = setInterval(autoPlayNext,2000); 
		$(targetParent+' .TimePlayer').off('click',autoPlayHistory); 
		$(targetParent+' .TimePlayer i.fa').removeClass('fa-play').addClass('fa-pause'); 
		$(targetParent+' .TimePlayer').on('click',stopAutoPlayHistory); 
		eventListeners.autoplayStart(); 
	}
	function stopAutoPlayHistory(){ 
		clearInterval(autoPlayInterval); 
		$(targetParent+' .TimePlayer').off('click',stopAutoPlayHistory); 
		$(targetParent+' .TimePlayer').on('click',autoPlayHistory); 
		$(targetParent+' .TimePlayer i.fa').removeClass('fa-pause').addClass('fa-play'); 
		eventListeners.autoplayStop(); 
	} 
	function autoPlayNext(){ 
		var handles = $(targetParent+' .buttons a'); 
		var current_id = $(targetParent+' .buttons a.current')[0].id; 
		var ids = []; 
		var current_index; 
		for(var i=0,il=handles.length;i<il;i++){ 
			ids[i] = handles[i].id; 
			if(handles[i].id==current_id) current_index=i; 
		} 
		
		var next_index = current_index; 
		
		do{ 
			next_index = next_index+1; 
			if(next_index == ids.length) next_index = 0; 
		}while($(handles[next_index]).hasClass('unavailable')); 
		
		eventListeners.autoplayAdvance(); 
		$('#'+ids[next_index]).trigger('click'); 
	} 
	function showTimescaleTip(){ 
		if(!$(this).hasClass("unavailable")){ 
			$('#TimescaleTooltip span').html($(this).data('enddate')); 
			$('#TimescaleTooltip').css('top',$(this).offset().top+15).css('left',$(this).offset().left-30).fadeIn(); 
		}
	} 
	function hideTimescaleTip(){ 
		$('#TimescaleTooltip').hide(); 
	} 
	function showTimeScaleInfo(){ 
		$('#TimescaleInfotip').css('top',$(this).offset().top+20).css('left',$(this).offset().left-15).fadeIn(); 	
	} 
	function hideTimeScaleInfo(){ 
		$('#TimescaleInfotip').fadeOut(); 
	} 
	
	this.startAutoPlay = autoPlayHistory; 
	this.stopAutoPlay = stopAutoPlayHistory; 
	
} 