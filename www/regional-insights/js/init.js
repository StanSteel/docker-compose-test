var aWhereRegionalInterface = function(){ 
	if(window.aWhere==undefined) window.aWhere = {};
	if(aWhere.config==undefined) aWhere.config = {}; 
	
	// Map Type Definitions: Creates Human-Friendly names and descriptions for each metric
	// Includes mechanism to hide a particular metric
	// Includes mechanism for grouping maps in categories (used in the download) 
	// If new metrics are added to the source images, they should be ignored unless defined here and set show:true
	aWhere.config.ChartsMapsDefinition = { 
		'label_temp':{
			'name':'Temperature',
			'show':true,
			'isGroup':true,
			'description':''},
		'CurrentAvgMaxT':{
			'name':'Average High Temperature',
			'show':true,
			'isGroup':false,
			'description':'The average high temperature over the previous four weeks.'},
		'CurrentAvgMinT':{
			'name':'Average Low Temperature',
			'show':true,
			'isGroup':false,
			'description':'The average low temperature over the previous four weeks.'},
		'DiffAvgMaxTFromLTN':{
			'name':'Difference in Average High Temp from Norms',
			'show':false,
			'isGroup':false,
			'description':'Comparing the average high temperature from the previous four weeks to the long-term average of the same days over the previous 10 years.'},
		'DiffAvgMinTFromLTN':{
			'name':'Difference in Average Low Temp from Norms',
			'show':false,
			'isGroup':false,
			'description':'Comparing the average low temperature from the previous four weeks to the long-term average of the same days over the previous 10 years.'},
		'DiffAvgMaxTFromLastYear':{
			'name':'Difference in Average Max Temp from Last Year',
			'show':false,
			'isGroup':false,
			'description':'Comparing the average high temperature from the previous four weeks to the long-term average of the same days last year.'},
		'DiffAvgMinTFromLastYear':{
			'name':'Difference in Average Min Temp from Last Year',
			'show':false,
			'isGroup':false,
			'description':'Comparing the average low temperature from the previous four weeks to the long-term average of the same days last year.'},
		'DiffPCTAvgMaxTFromLastYear':{
			'name':'% Difference in Average Max Temp from Last Year',
			'show':false,
			'isGroup':false,
			'description':'Comparing the average high temperature from the previous four weeks to the long-term average of the same days last year, expressed as a percentage.'},
		'DiffPCTAvgMinTFromLastYear':{
			'name':'% Difference in Average Min Temp from Last Year',
			'show':false,
			'isGroup':false,
			'description':'Comparing the average low temperature from the previous four weeks to the long-term average of the same days last year, expressed as a percentage.'},
		'DiffPCTAvgMaxTFromLTN':{
			'name':'% Difference in Average Max Temp from Norms',
			'show':false,
			'isGroup':false,
			'description':'Comparing the average high temperature from the previous four weeks to the long-term average of the same days last year, expressed as a percentage.'},
		'DiffPCTAvgMinTFromLTN':{
			'name':'% Difference in Average Min Temp from Norms',
			'show':false,
			'isGroup':false,
			'description':'Comparing the average low temperature from the previous four weeks to the long-term average of the same days last year, expressed as a percentage.'},
 
		'label_precip':{
			'name':'Precipitation',
			'show':true,
			'isGroup':true,
			'description':''},
		'CurrentSumPrec':{
			'name':'Total Precipitation',
			'show':true,
			'isGroup':false,
			'description':'Total preciptiation received over the previous four weeks, in millimeters.'},
		'DiffSumPrecFromLTN':{
			'name':'Difference in Precipitation from Norms',
			'show':true,
			'isGroup':false,
			'description':'Comparing the last four weeks of precipitation to the 10-year averages of the same days. Figures closer to the extremes indicate much drier conditions (red) or much wetter conditions (green/blue).'},
		'DiffSumPrecFromLastYear':{
			'name':'Difference in Precipitation from Last Year',
			'show':true,
			'isGroup':false,
			'description':'Comparing the last four weeks of precipitation to last year. Figures closer to the extremes indicate much drier conditions (red) or much wetter conditions (green/blue).'},
		'DiffPCTSumPrecFromLTN':{
			'name':'% Difference in Precipitation from Norms',
			'show':true,
			'isGroup':false,
			'description':'Comparing the last four weeks of precipitation to the 10-year averages of the same days, expressed as a percentage difference. Figures closer to the extremes indicate much drier conditions (red) or much wetter conditions (green/blue).'},
		'DiffPCTSumPrecFromLastYear':{
			'name':'% Difference in Precipitation from Last Year',
			'show':true,
			'isGroup':false,
			'description':'Comparing the last four weeks of precipitation to last year, expressed as a percentage difference. Figures closer to the extremes indicate much drier conditions (red) or much wetter conditions (green/blue).'},

		'label_ppet':{
			'name':'P/PET',
			'show':true,
			'isGroup':true,
			'description':''},
		'CurrentAvgSumPOverSumPET':{
			'name':'Current P/PET',
			'show':true,
			'isGroup':false,
			'description':'P/PET is the ratio of Precipitation to Potential Evapotranspiration, and represents an abundance or lack of available water to the plants in a region. These charts and maps show the total P/PET over the last four weeks. Figures below 1 (shown in orange/red) indicate not enough rain to balance the amount of water used/evaporating. Figures above 1 (shown in green/blue) indicate high water availability.'},
		'DiffAvgSumPOverSumPETFromLTN':{
			'name':'P/PET Difference from Norms',
			'show':true,
			'isGroup':false,
			'description':'Comparing the last four weeks\' total P/PET to the 10-year averages of the same days. P/PET represents an abundance or lack of available water to the plants in a region. Figures closer to the extremes indicate much drier conditions (red) or much wetter conditions (green/blue).'},
		'DiffAvgSumPOverSumPETFromLastYear':{
			'name':'P/PET Difference from Last Year',
			'show':true,
			'isGroup':false,
			'description':'Comparing the last four weeks\' total P/PET to last year. P/PET represents an abundance or lack of available water to the plants in a region. Figures closer to the extremes indicate much drier conditions (red) or much wetter conditions (green/blue).'},
		'DiffPCTP_PETFromLTN':{
			'name':'P/PET % Difference from Norms',
			'show':true,
			'isGroup':false,
			'description':'Comparing the last four weeks\' total P/PET to the 10-year averages of the same days, expressed as a percentage difference. P/PET represents an abundance or lack of available water to the plants in a region. Figures closer to the extremes indicate much drier conditions (red) or much wetter conditions (green/blue).'},
		'DiffPCTP_PETFromLastYear':{
			'name':'P/PET % Difference from Last Year',
			'show':true,
			'isGroup':false,
			'description':'Comparing the last four weeks\' total P/PET to last year, expressed as a percentage difference. P/PET represents an abundance or lack of available water to the plants in a region.'},

		'label_gdd':{
			'name':'Growing Degree-Days',
			'show':false,
			'isGroup':true,
			'description':''},
		'CurrentSumGDD5095':{
			'name':'Current Growing Degree-Days (Base:50&deg;C; Cap:95&deg;C)',
			'show':false,
			'isGroup':false,
			'description':'Growing Degree-Days are a unit of heat measurement which when aggregated, correspond to the likely growth stage of a crop. Low GDDs on average will mean slower growing crops, where as very high GDD indicates an abundance of heat that may decrease P/PET or harm the crop. These images represent the total GDD accumulated over the last four weeks. '},
		'DiffPCTSumGDD5095FromLTN':{
			'name':'% Difference in GDD from Norms (Base:50&deg;C; Cap:95&deg;C)',
			'show':false,
			'isGroup':false,
			'description':'Comparing the last four weeks\' total GDD to the 10-year averages of the same days, expressed as a percentage difference.'},
		'DiffPCTSumGDD5095FromLastYear':{
			'name':'% Difference in GDD from Last Year (Base:50&deg;C; Cap:95&deg;C)',
			'show':false,
			'isGroup':false,
			'description':'Comparing the last four weeks\' total GDD to last year, expressed as a percentage difference.'},
		'DiffSumGDD5095FromLTN':{
			'name':'Difference in GDD from Norms',
			'show':false,
			'isGroup':false,
			'description':'Comparing the last four weeks\' total GDD to the 10-year averages of the same days.'},
		'DiffSumGDD5095FromLastYear':{
			'name':'Difference in GDD from Last Year',
			'show':false,
			'isGroup':false,
			'description':'Comparing the last four weeks\' total GDD to last year.'},

		'label_risk':{
			'name':'Risk Scores',
			'show':true,
			'isGroup':true,
			'description':'' },
		'Z_SumPET':{
			'name':'PET Risk Score',
			'show':true,
			'isGroup':false,
			'description':''},
		'Z_SumPOverSumPET':{
			'name':'P/PET Risk Score',
			'show':true,
			'isGroup':false,
			'description':''},
		'Z_SumPrec':{
			'name':'Precipitation Risk Score',
			'show':true,
			'isGroup':false,
			'description':''},
	};

	//Region Name Translation
	//The source images use different names for each region, this converts them to the same context/syntax across all.
	aWhere.config.CountryNameDefinition = {
		"Argentina":"Argentina",
		"Brazil":"Brazil",
		"BurkinaFaso":"Burkina Faso",
		"CentralEastAfrica":"Central East Africa",
		"China":"China",
		"Colombia":"Colombia",
		"Ethiopia":"Ethiopia",
		"Europe":"Europe",
		"Guatemala":"Guatemala",
		"Honduras":"Honduras",
		"India":"India",
		"Indonesia":"Indonesia",
		"Mali":"Mali",
		"Mexico":"Mexico",
		"MexicotoPanama":"Central America",
		"Nicaragua":"Nicaragua",
		"Niger":"Niger",
		"Nigeria":"Nigeria",
		"NorthernAfrica":"Northern Africa",
		"SahelAfrica":"Sahel",
		"SouthAfrica":"South Africa",
		"Thailand":"Thailand",
		"Uganda":"Uganda",
		"Ukraine":"Ukraine",
		"USGrains":"USA Grain Region",
		"Vietnam":"Vietnam",
		"WesternAfrica":"Western Africa"
	};
	
	aWhere.config.CountryRegionOrganization = {
		"Americas":[
			"Argentina",
			"Brazil",
			"Colombia",
			"Guatemala",
			"Honduras",
			"Mexico",
			"MexicotoPanama",
			"Nicaragua",
			"USGrains"
		],
		"Europe":[
			"Europe"
		],
		"Africa":[
			"BurkinaFaso",
			"CentralEastAfrica",
			"Ethiopia",
			"Mali",
			"Niger",
			"Nigeria",
			"NorthernAfrica",
			"SahelAfrica",
			"SouthAfrica",
			"Uganda",
			"WesternAfrica"
		],
		"Asia":[
			"China",
			"India",
			"Indonesia",
			"Thailand",
			"Ukraine",
			"Vietnam"
		]
	};

	
	aWhere.CountryPicker = new aWhereCountryPicker(); 
	aWhere.CountryPicker.init('Map',['Crops']); 
	aWhere.CountryInterface = new aWhereCountryInterface(); 
	aWhere.CountryInterface.init(); 
	
	// Start Screen Setup
	
	$('#Map').css('left',$(window).width()*.25).css('top',200); 

	$( document ).ready(function(){ 
		$('#Loading').hide(); 
		$('#Map').css('opacity',1);
		$('#Map').click(); 
	}); 
	
}(); 
