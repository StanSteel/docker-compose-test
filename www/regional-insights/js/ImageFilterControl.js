var aWhereImageFilterControl = function(owner,targetParent){ 

	var eventListeners = { 
							selectionChange: function(){ },
	 					 }; 
	
	var chooserType; 
	
	this.create = function(config){ 
		
		chooserType = config.chooserType; 
		
		$(targetParent).append('<div id="'+chooserType+'ChooserWrapper"><label for="'+chooserType+'Chooser">'+config.chooserLabel+'</label><select id="'+chooserType+'Chooser"></select></div>'); 

		
		var optgroup = null,thisitem;
		for(var i=0,il=config.items.length;i<il;i++){ 
			if(config.items[i].type=='group'){ 
				if(optgroup!==null) $('#'+chooserType+'Chooser').append(optgroup); 
				optgroup = $('<optgroup></optgroup>').attr('label',config.items[i].label); 
				continue; 
			}
			thisitem = $("<option></option>").attr("value",config.items[i].value).text(config.items[i].label); 
			if(optgroup!==null){ 
				optgroup.append(thisitem); 
			} else { 
				$('#'+chooserType+'Chooser').append(thisitem); 
			} 
		}
		if(optgroup!==null){ 
			$('#'+chooserType+'Chooser').append(optgroup);
		} 
	
		$('#'+chooserType+'Chooser').on('change',changeEvent); 
	
	} 
	this.destroy = function(config){ 
		$('#'+chooserType+'Chooser').off('change',changeEvent); 
		$("#"+chooserType+"ChooserWrapper").remove(); 
	} 
	
	this.setListener = function(e,func){ 
		eventListeners[e] = func; 
	} 
	this.getValue = function(){ 
		return $('#'+chooserType+'Chooser').val(); 
	} 
	
	function changeEvent(){ 
		eventListeners.selectionChange({'event':'selectionChange','chooserType':chooserType,'newValue':$(this).val()}); 
	} 
}