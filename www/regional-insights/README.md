# Regional Insights Demo Tools
This repository contains all code for the Regional Insights Demo:

![](res/img/InsightsDemo.JPG)

## Running locally
1. Clone this repo locally
2. Place the repo inside of the `htdocs` folder of  whatever HTTP server you are using (Apache, NGINX, etc)
3. With your appropriate web server running, navigate to the `regional-insights` directory. 

Example:

``` 
http://localhost:80/regional-insights/
```

## Important Note:
#### Regional Insight Demo
This repository only contains one working country, **Vietnam**. This is the **only country** you will be able to view in the Regional Insight demo. 

If you'd like to add additional images for other countries you can download sample data, located [here](https://bitbucket.org/dallegrezza/regional-insights-images/get/25539b656153.zip). Download and unzip the images to the `charts-maps` directory, then refresh your browser.

## Original Documentation
The original documentation for this repository is below.


### Loading Maps and Charts
Currently, the maps and charts in the Regional Insight Tool are manually uploaded to the Explore server. The system relies on the consistent naming pattern; when the site is loaded it scans through the maps and charts directory to create the list of currently available items, and parses the file names to determine the country, crop, metric type, and timeframe.  There is (now) error handling to skip any new naming patterns that might emerge. 

####Process

1.	When Dan shares the weekly maps and charts, I go into Sharepoint to download them all to my computer (see list of countries that I download, below).

2.	Unzip all the downloaded files and discard the zips.

3.	Use scp to copy the files en masse up to the explore box (assuming all the files are grouped into a directory called ‘maps’):

	`scp -r maps USER@ec2dlxlmp001prd.eus.awhere.com:`

This will upload everything in the `maps` folder to the user home directory on the explore server.

4.	SSH into the Explore box

5.	Move the maps and charts into the webroot directory for the website. Note, if you followed the instructions above, the files may in a directory called ‘maps.’  You need to move the contents of this directory into the target directory:

	`mv maps/* /var/www/explore-awhere/explore/regional/charts-maps`

6.	You may need to set permissions on the new images. From my mac, the default is to limit the image ownership permissions to the owner only, but this makes them inaccessible to the webserver and public in general. Therefore I usually run this command for good measure:

	`sudo chmod –R 755 /var/www/explore-awhere/explore/regional/charts-maps/*`

At that point, the Regional Insights Tool should pick up the new images immediately.
 
#### Clean Up

We’re getting to the point where we need to start deleting the oldest files on a weekly basis. The UI has a limited amount of space to fit the timeline, so within the next few weeks we should begin deleting the oldest ones whenever new ones are added.

#### Files to Download

More countries/regions are analyzed in Dan’s script than are included in the Tool. The ones to download on a weekly basis are as follows. Note that if others are included and added to the server this won’t break anything, they’ll just be ignored.


- Argentina
- Brazil
- BurkinaFaso
- CentralEastAfrica
- China
- Colombia
- Ethiopia
- Europe
- Guatemala
- Honduras
- India
- Indonesia
- Mali
- MalitoNigeria
- MexicotoPanama
- Nicaragua
- Niger
- Nigeria
- NorthernAfrica
- SahelAfrica
- SouthAfrica
- Thailand
- Uganda
- Ukraine
- USGrains
- Vietnam
- WesternAfrica