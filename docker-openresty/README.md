# docker-openresty - Docker tooling for OpenResty

## Supported tags and respective `Dockerfile` links (removed all except Debian Jessie for now) 

- [`jessie`, (*jessie/Dockerfile*)](https://github.com/openresty/docker-openresty/blob/master/jessie/Dockerfile)


Table of Contents
=================

* [Description](#description)
* [Usage](#usage)
* [Supported image base systems](#Supported-base-systems)
* [OPM](#opm)
* [LuaRocks](#luarocks)
* [Docker ENTRYPOINT](#docker-entrypoint)

Description
===========

`docker-openresty` is [Docker](https://www.docker.com) tooling for OpenResty (https://www.openresty.org).

Docker is a container management platform.

This is an aWhere fork of https://github.com/openresty/docker-openresty and is intended to be used as a template for other applications that could use Openresty and Docker.


Usage
=====

***Note*** These instructions assume that the docker image being built will be named "awhere-openresty" and that the running container will be named "awhere-openresty1". Feel free to substitute any name you like.

To clone the project and build the image as-is, run the following:
```
git clone https://<Bitbucket Username>@bitbucket.org/awhere/docker-openresty.git
cd docker-openresty
docker build -t awhere-openresty -f jessie/Dockerfile .
```
The command below will start a container named awhere-openresty1 with a fully working terminal. Omit the --tty=true if you will not be using the terminal.
```
docker run --tty=true --name "awhere-openresty1" awhere-openresty
```
Other useful options when running the container would be things like -p to map ports, -v to map volumes, and -d to daemonize.

The container should now be running. To test, browse to http://<Container-IP> .  The container's IP may be determined with docker inspect:
```
docker inspect <container ID or name> | grep IPAddress
```
*** HyperV users*** You may need to use the IP of the HyperV VM instead of the one produced by the above command, and you will likely have needed to map a port when running the container. 

The Openresty error and access logs will be output straight to the command prompt where you started the container. If you need to get a shell on the running container, you may issue the following command in another window:
```
docker exec -it awhere-openresty1  /bin/bash
```

Any files in nginx/conf/ or nginx/html/ will be copied to the appropriate openresty directory within the image. This behavior is defined at the end of the Dockerfile, and more locations (for lua scripts, etc) may be cofigured at a later date. 

A table containing optional build-time options may be found at the bottom of this document.

Supported image base systems
================
Dockerfiles are currently only provided for the following base systems(possibly more to come), selecting the Dockerfile path with `-f`:

Debian Jessie: `-f jessie/Dockerfile`

OPM Packages
===

Starting at version 1.11.2.2, OpenResty includes a [package manager called `opm`](https://github.com/openresty/opm#readme), which can be found at `/usr/local/openresty/bin/opm`.

You may visit https://opm.openresty.org/ to view the packages available for installation via OPM

LuaRocks Packages
========

[LuaRocks](https://luarocks.org/) is included as well

It is available at `/usr/local/openresty/luajit/bin/luarocks`.  Packages can be added in your dependent Dockerfiles like so:

```
RUN /usr/local/openresty/luajit/bin/luarocks install <rock>
```

Docker ENTRYPOINT
=================

The `-g "daemon off;"` directive is used in the Dockerfile ENTRYPOINT to keep the Nginx daemon running after container creation. If this directive is added to the nginx.conf, then it may be omitted from the ENTRYPOINT.

To invoke with another ENTRYPOINT, for example the `resty` utility, invoke like so:

```
docker run [options] --entrypoint /usr/local/openresty/bin/resty openresty/openresty:xenial [script.lua]
```

Build Options
=================
The following are the available build-time options. They can be set using the `--build-arg` CLI argument, like so:

```
docker build --build-arg RESTY_J=4 -f jessie/Dockerfile .
```

| Key | Default | Description |
:----- | :-----: |:----------- |
|RESTY_VERSION | 1.11.2.2 | The version of OpenResty to use. |
|RESTY_LUAROCKS_VERSION | 2.3.0 | The version of LuaRocks to use. |
|RESTY_OPENSSL_VERSION | 1.0.2j | The version of OpenSSL to use. |
|RESTY_PCRE_VERSION | 8.39 | The version of PCRE to use. |
|RESTY_J | 1 | Sets the parallelism level (-jN) for the builds. |
|RESTY_CONFIG_OPTIONS | "--with-file-aio --with-http_addition_module --with-http_auth_request_module --with-http_dav_module --with-http_flv_module --with-http_geoip_module=dynamic --with-http_gunzip_module --with-http_gzip_static_module --with-http_image_filter_module=dynamic --with-http_mp4_module --with-http_perl_module=dynamic --with-http_random_index_module --with-http_realip_module --with-http_secure_link_module --with-http_slice_module --with-http_ssl_module --with-http_stub_status_module --with-http_sub_module --with-http_v2_module --with-http_xslt_module=dynamic --with-ipv6 --with-mail --with-mail_ssl_module --with-md5-asm --with-pcre-jit --with-sha1-asm --with-stream --with-stream_ssl_module --with-threads" | The options to pass to OpenResty's `./configure` script. |

[Back to TOC](#table-of-contents)
